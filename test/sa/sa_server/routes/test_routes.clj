(ns sa.sa-server.routes.test-routes
  (:require [cheshire.core               :as cheshire]
            [clojure.test                :refer [is deftest test-ns testing]]
            [ring.mock.request           :as mock]
            [clojure.edn                 :as edn]
            [sa.sa-server.routes.models  :as rm]
            [clojure.string              :as str]
            [clojure.test                :refer :all]
            [sa.sa-store.store-factory   :as sf]
            [sa.sa-server.config]
            [sa.sa-store.couch-based]))  ;This causes the couch db to be initialised.

(def sample-1 {:sadf/meta {:sadf/name "Test Model 1"
                           :sadf/uuid "123"
                           :sadf/summary "This is a test model with id 123"
                           :sadf/detail {:sadf/ownership "Owened by dept q"
                                         :sadf/contact   "Fred Smith"
                                         :sadf/contact-email "a@b.com"
                                         :sadf/uri "a-scheme://a-server:123/api/model/123"}}
               :sadf/model {}
               :sadf/layout {}
               :sadf/context-objects {}})

(def sample-2 {:sadf/meta {:sadf/name "Test Model 2"
                           :sadf/uuid "123"
                           :sadf/summary "This is a test model with id 123"
                           :sadf/detail {:sadf/ownership "Owened by dept q"
                                         :sadf/contact   "Fred Smith"
                                         :sadf/contact-email "a@b.com"
                                         :sadf/uri "a-scheme://a-server:123/api/model/123"}}
               :sadf/model {}
               :sadf/layout {}
               :sadf/flows []
               :sadf/context-objects {}})

(defn parse-body [body]
  (cheshire/parse-string (slurp body) true))

(defn parse-edn-response [response]
  (edn/read-string (:body response)))

(defn response-content-type [response]
  (get-in response [:headers "Content-Type"]))

(deftest basic-models

  (testing "Test POST then GET request to /api/model"
    ;(with-redefs [sa.sa-server.config/store-type ::sf/file-store]

      (let [post-response (rm/model (-> (mock/request :post "/api/model")
                                        (mock/body (str sample-1))
                                        (mock/header "Content-Type" "application/edn")))
            get-response  (rm/model (-> (mock/request :get "/api/model")
                                        (mock/header :Accept "application/edn")))]


        (let [uri           (->> get-response
                                 parse-edn-response
                                 first
                                 (new java.net.URI))]

          (is (= (:status post-response) 303)) ;see other
          (is (= (:status get-response)  200))

          ;edn get
          (let [edn-get-response (rm/model (-> (mock/request :get  (. uri getPath))
                                               (mock/header :Accept "application/edn")))]

            (is (= (:status edn-get-response) 200))
            (is (true? (-> edn-get-response
                           response-content-type
                           (str/includes? "application/edn")))))

          ;json get
          (let [json-get-response (rm/model (-> (mock/request :get  (. uri getPath))
                                                (mock/header :Accept "application/json")))]

             (is (= (:status json-get-response) 200))
             (is (true? (-> json-get-response
                            response-content-type
                            (str/includes? "application/json")))))


          ;transit json get
          (let [tr-get-response (rm/model (-> (mock/request :get  (. uri getPath))
                                              (mock/header :Accept "application/transit+json")))]

             (is (= (:status tr-get-response) 200))
             (is (true? (-> tr-get-response
                            response-content-type
                            (str/includes? "application/transit+json")))))

          ;unknown accept type
          (let [tr-get-response (rm/model (-> (mock/request :get  (. uri getPath))
                                              (mock/header :Accept "application/transit+edn")))]
             (is (= (:status tr-get-response) 406)))))))

(deftest delete-model
  (testing "Test POST then GET then DELETE request to /api/model"
    ;(with-redefs [sa.sa-server.config/store-type ::sf/file-store]
     (let [post-response (rm/model (-> (mock/request :post "/api/model")
                                       (mock/body (str sample-1))
                                       (mock/header "Content-Type" "application/edn")))
           get-response  (rm/model (-> (mock/request :get "/api/model")
                                       (mock/header  :Accept "application/edn")))
           uri           (->> get-response
                              parse-edn-response
                              first
                              (new java.net.URI))]
        (println (str ">>>>URI>>>" uri))
        (is (= (:status post-response) 303)) ;see other
        (is (= (:status get-response)  200))

        ;delete
        (let [delete-response (rm/model (-> (mock/request :delete  (. uri getPath))
                                            (mock/header :Accept "application/edn")))
              delete-entity   (->> delete-response
                                   parse-edn-response)
              get-response    (rm/model (-> (mock/request :get (. uri getPath))
                                            (mock/header :Accept "application/edn")))]
          (is (= (:status delete-response) 200)) ;Should have the id of the deleted entity in the response
          (is (= (:status get-response)    404))))))

(deftest update-model
  (testing "get then delete all then post then update using put then get the updated model back"
    ;(with-redefs [sa.sa-server.config/store-type ::sf/file-store]
      (let [get-response  (rm/model (-> (mock/request :get "/api/model")
                                        (mock/header :Accept "application/edn")))

            uris           (for [resp (->> get-response
                                           parse-edn-response)]
                             (new java.net.URI resp))

            dels           (for [uri uris]
                             (rm/model (-> (mock/request :delete  (. uri getPath)))))

            post-response   (rm/model (-> (mock/request :post "/api/model")
                                          (mock/body (str sample-2))
                                          (mock/header "Content-Type" "application/edn")
                                          (mock/header :Accept "application/edn")))


            uri             (get-in post-response [:headers "Location"])
            uuid            (get-in (parse-edn-response post-response) [:sadf/meta :sadf/uuid])]
        (let [updated-sample-2 (-> sample-2 (assoc :sadf/model {:procs [{:name "Fred"}]}))
                                            ; (assoc))

              updated          (rm/model (-> (mock/request :put uri)
                                             (mock/body  (str updated-sample-2))
                                             (mock/header "Content-Type" "application/edn")
                                             (mock/header :Accept "application/edn")))]
          (is (= (:status updated) 409) "The update should fail as the model uuid of te update is not correct")

          (let [updated-sample-2 (-> sample-2 (assoc :sadf/model {:procs [{:name "Fred"}]})
                                              (assoc-in [:sadf/meta :sadf/uuid] uuid))
                updated          (rm/model (-> (mock/request :put uri)
                                               (mock/body  (str updated-sample-2))
                                               (mock/header "Content-Type" "application/edn")
                                               (mock/header :Accept "application/edn")))

                new-sample-2    (->> (rm/model (-> (mock/request :get uri)
                                                   (mock/header :Accept "application/edn"))))]


            (is (= (parse-edn-response new-sample-2) updated-sample-2)))))))



(deftest test-summaries
  (testing "Test get request to /api/model/summary - first remove any models, then add one."
    ;(with-redefs [sa.sa-server.config/store-type ::sf/file-store]
     (let [get-response  (rm/model (-> (mock/request :get "/api/model")
                                       (mock/header :Accept "application/edn")))
           uris          (doall (for [resp (->> get-response
                                                parse-edn-response)]
                                  (new java.net.URI resp)))

           dels          (doall (for [uri uris]
                                   (rm/model (-> (mock/request :delete  (. uri getPath))))))


           get-response1  (parse-edn-response (rm/model (-> (mock/request :get "/api/model")
                                                            (mock/header :Accept "application/edn"))))]
        ;Make sure there are no models
        (is (= 0 (count get-response1)))

        (let [post-response (parse-edn-response (rm/model (-> (mock/request :post "/api/model")
                                                              (mock/body (str sample-2))
                                                              (mock/header "Content-Type" "application/edn")
                                                              (mock/header :Accept "application/edn"))))]
          ;Check the post response
          (is (= "This is a test model with id 123" (get-in post-response [:sadf/meta :sadf/summary])))

          (let [summaries       (->> (rm/model (-> (mock/request :get "/api/model/summary")
                                                   (mock/header  :Accept "application/edn")))
                                     parse-edn-response)

                summary         (first (filter #(= (:sadf/name %) "Test Model 2") summaries))
                updated-summary (assoc summary  :sadf/name "My Test Model")
                uuid            (:sadf/uuid summary)]

            (let [updated         (rm/model (-> (mock/request :put (str "/api/model/summary/" uuid))
                                                (mock/body  (str updated-summary))
                                                (mock/header "Content-Type" "application/edn")
                                                (mock/header :Accept "application/edn")))
                  new-summary     (->> (rm/model (-> (mock/request :get (str "/api/model/summary/" uuid))
                                                     (mock/header :Accept "application/edn")))
                                       parse-edn-response)]
              (is (= (:sadf/name updated-summary) (:sadf/name new-summary)) "Did the stored summary get updated?")))))))
