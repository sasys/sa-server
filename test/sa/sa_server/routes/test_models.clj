(ns sa.sa-server.routes.test-models
  (:require [cognitect.transit           :as t]
            [clojure.test                :refer [is deftest test-ns]]
            [sa.sa-server.routes.models  :refer [parse-json->transit build-entry-url parse-transit parse-json parse-edn make-model-summary parse-body check-content-type]]
            [sa.sa-store.couch-based])  ;This causes the couch db to be initialised.
  (:import [java.io ByteArrayOutputStream]))

(def layout-1-1 {(keyword "sadf" "0")   [450 450]})
(def layout-1-2 {"sadf/0"  [450 450]})

(def key-1 :res)

(def sample-1 {:sadf/meta {:sadf/name "Test Model 1"
                           :sadf/uuid "123"
                           :sadf/summary "This is a test model with id 123"
                           :sadf/detail {:sadf/ownership "Owened by dept q"
                                         :sadf/contact   "Fred Smith"
                                         :sadf/contact-email "a@b.com"}}
               :sadf/model {}
               :sadf/layout {}})

(def good-parser-map {"application/json" parse-json "application/edn" parse-edn "application/transit+json" parse-transit})
(def empty-parser-map {})

(def good-content-types ["application/json" "application/edn" "application/transit+json"])

(deftest test-make-model-summary
  (let [test-model {:sadf/meta {:sadf/uuid "123456"}}
        test-request {:scheme "a-scheme" :server-name "a-server" :server-port 123}
        summary (make-model-summary test-request test-model)]
    (is (= summary {:sadf/uuid "123456"
                    :sadf/uri  "a-scheme://a-server:123/api/model/123456"
                    :sadf/detail {:sadf/uri "a-scheme://a-server:123/api/model/123456"}}))))




(deftest test-parse-edn
  (let [test-edn-vec [{:name "fred"}]
        test-edn-no-vec {:name "bud"}
        context-vec {:request {:body (str test-edn-vec)}}
        context-no-vec {:request {:body (str test-edn-no-vec)}}
        context-no-body {:request {:body nil}}
        key :data
        parsed-vec (parse-edn context-vec key)
        parsed-no-vec (parse-edn context-no-vec key)
        parsed-no-body (parse-edn context-no-body key)]
    (is (= [false {:data {:name "fred"}} parsed-vec]))
    (is (= [false {:data {:name "bud"}} parsed-no-vec]))
    (is (= [false {:data {:name "bud"}} parsed-no-vec]))))




(deftest parse-transit-keyword
  (let [body-1-1 (str (merge sample-1 {:sadf/layout layout-1-1}))
        out-1-1  (ByteArrayOutputStream. 4096)
        writer   (t/writer out-1-1 :json)]
    (t/write writer body-1-1)
    (let [context-1-1 {:request {:body (.toByteArray out-1-1)}}]
      (is (= [false {:res (merge sample-1 {:sadf/layout layout-1-1})} (parse-transit context-1-1 key-1)])))))

(deftest parse-transit-string-keyword
  (let [body-1-2 (str (merge sample-1 {:sadf/layout layout-1-2}))
        out-1-2  (ByteArrayOutputStream. 4096)
        writer   (t/writer out-1-2 :json)]
    (t/write writer body-1-2)
    (let [context-1-2 {:request {:body (.toByteArray out-1-2)}}]
      (is (= [false {:res (merge sample-1 {:sadf/layout layout-1-2} (parse-transit context-1-2 key-1))}])))))


(deftest parse-transit-print
  (let [body-1-1 (str (merge sample-1 {:sadf/layout layout-1-1}))
        out-1-1  (ByteArrayOutputStream. 4096)
        writer   (t/writer out-1-1 :json)]
    (t/write writer body-1-1)
    (let [context-1-1 {:request {:body (.toByteArray out-1-1)}}
          parsed (parse-transit context-1-1 key-1)]
      (is (= (first parsed) false))
      (is (= (:sadf/layout  layout-1-1))))))


(deftest test-parse-body
  (let [test-edn-vec [{:name "fred"}]
        put-context  {:request {:request-method :put  :body (str test-edn-vec) :headers {"content-type" "application/edn"}}}
        post-context {:request {:request-method :post :body (str test-edn-vec) :headers {"content-type" "application/edn"}}}
        parser-key :data
        parsed-1 (parse-body good-parser-map put-context parser-key)
        parsed-2 (parse-body good-parser-map post-context parser-key)]
    (is (= [false {:data {:name "fred"}}] parsed-1))
    (is (= [false {:data {:name "fred"}}] parsed-2))))

(deftest test-check-content-type
  (let [good-1 "application/json"
        good-2 "application/edn"
        good-3 "application/transit+json"
        bad-1  "application/not-me"
        put-context-1  {:request {:request-method :put  :headers {"content-type" good-1}}}
        post-context-2 {:request {:request-method :post :headers {"content-type" good-2}}}
        put-context-3  {:request {:request-method :put  :headers {"content-type" good-3}}}
        post-context-4 {:request {:request-method :post :headers {"content-type" bad-1}}}]
    (is (= "application/json" (check-content-type put-context-1 good-content-types)))
    (is (= "application/edn" (check-content-type post-context-2 good-content-types)))
    (is (= "application/transit+json" (check-content-type put-context-3 good-content-types)))
    (is (= [false {:message "Unsupported Content-Type"}] (check-content-type post-context-4 good-content-types)))))


(deftest test-build-entry-url
  (let [req-1 {:scheme "https" :server-name "serv-1" :server-port "99" :uri "/api/model"}]
    (is (= "https://serv-1:99/api/model/22" (str (build-entry-url req-1 22))))))


(deftest test-parse-json->transit
  (let [body1 "{\"name\" : \"your-model-1001\"}"
        bad-body1 ""
        req1 {:body body1 :scheme "https" :server-name "serv-1" :server-port "99" :uri "/api/model"}
        bad-req1 (assoc req1 :body bad-body1)
        ctx1 {:request req1}
        bad-ctx1 {:requst bad-req1}]
    (is (= [false {::data  "{\"~#'\":\"{\\\"name\\\" : \\\"your-model-1001\\\"}\"}"}] (parse-json->transit ctx1 ::data)))
    (is (= {:message "No body"} (parse-json->transit bad-ctx1 ::data)))))
