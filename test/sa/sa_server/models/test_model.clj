(ns sa.sa-server.models.test-model
  (:require [clojure.test                :refer [is deftest test-ns]]
            [sa.sa-server.models.model   :refer [summarise-all-models path make-uri-string delete-all summarise-all new-model get-model-by-name update-model list-model-names update-summary]]
            [clojure.string              :refer [starts-with? split]]
            [sa.sa-store.couch-based     :refer [make-db]]  ;This causes the couch db to be initialised.
            [sa.sa-server.routes.test-models]
            [sa.sa-server.routes.test-routes]))


;Test models
(def sample-1 {:sadf/meta {:sadf/name "Test Model 1"
                           :sadf/uuid "123"
                           :sadf/summary "This is a test model with id 123"
                           :sadf/detail {:sadf/ownership "Owened by dept q"
                                         :sadf/contact   "Fred Smith"
                                         :sadf/contact-email "a@b.com"}}
               :sadf/model {}})

(def sample-2 {:sadf/meta  {:sadf/name "Test Model 2"
                            :sadf/uuid "124"
                            :sadf/summary "This is a test model with id 124"
                            :sadf/detail  {:sadf/ownership "Owened by dept z bur soon to change long note"
                                           :sadf/contact   "Not yet assigned"
                                           :sadf/contact-email "z@b.com"}}
               :sadf/model {}})

(deftest test-summarise-all-models
  (let [test-model-1 {:sadf/meta {:sadf/uuid "123456"}}
        test-model-2 {:sadf/meta {:sadf/uuid "123"}}
        test-request {:scheme "a-scheme" :server-name "a-server" :server-port 123}
        summaries (summarise-all-models test-request [test-model-1 test-model-2])]
    (is (= (count summaries) 2))))

(def test-post-request
  {:scheme :http
   :server-name "test-server"
   :server-port 1234
   :uri "/api/model"})


(deftest uri-string
  (is (= "http://localhost:3000/api/model/1234-567" (make-uri-string "http" "localhost" 3000 path "1234-567"))))


(deftest updates
    (delete-all)
    (is (= true (-> (new-model (assoc-in sample-1 [:sadf/meta :sadf/name] "first-model") test-post-request)
                    (.toString)
                    (starts-with? "http://test-server:1234/api/model"))))
    (is (= true (first (apply vals (delete-all))))))

(deftest gets
  (let [_ (delete-all)
        model (assoc-in sample-1 [:sadf/meta :sadf/name] "another-model")
        uri (new-model model test-post-request)
        id  (last (split (.toString uri) #"/"))
        updated-model (assoc-in model [:sadf/meta :sadf/uuid] id)]

      (is (= "another-model"
             (get-in (get-model-by-name id) [:sadf/meta :sadf/name])))
      (is (= id (get-in (get-model-by-name id) [:sadf/meta :sadf/uuid])))
      (is (= {(keyword id) (assoc-in updated-model [:sadf/meta :sadf/summary] "New text.")}
             (update-model id (assoc-in updated-model [:sadf/meta :sadf/summary] "New text."))))
      (is (= {(keyword id) true} (first (delete-all))))))


(deftest lists
  (let [_ (delete-all)
        uri1 (new-model (assoc-in sample-1 [:sadf/meta :sadf/name] "first-model11") test-post-request)
        uri2 (new-model (assoc-in sample-1 [:sadf/meta :sadf/name] "first-model21") test-post-request)
        id1  (last (split (.toString uri1) #"/"))
        id2  (last (split (.toString uri2) #"/"))]
    (is (= (set (list id1 id2)) (set (list-model-names))))
    (is (= (set (list {(keyword id1) true} {(keyword id2) true})) (set (delete-all))))))



;TO DO WE NEED THESE TESTS BUT I CANT MAKE THEM WORK ON CIRCLE - WORK FINE ON LINUX AND OSX.
(deftest summaries
  (let [_ (delete-all)
        new1      (new-model (assoc-in sample-1 [:sadf/meta :sadf/name] "first-modelX") test-post-request)
        id        (last (split (.toString new1) #"/"))
        summaries (summarise-all)]
    (is (= id  (:sadf/uuid (first summaries))))
    (is (= "Test Model 1" (-> (update-summary id (:sadf/meta sample-1))  :sadf/name)))
    (is (= (list {(keyword id) true}) (delete-all)))))


(deftest sum-url
  (let [_   (delete-all)
        updated-model (assoc-in sample-1 [:sadf/meta :sadf/name] "first-modelX")
        new1      (new-model updated-model test-post-request)]

    (is (= true (starts-with?   (-> (summarise-all-models test-post-request [(:sadf/meta updated-model)])
                                    first
                                    :sadf/uri) "http://test-server:1234/api/")))
    (delete-all)))

(defn -main []
  (clojure.test/run-tests 'sa.sa-server.models.test-model
                          'sa.sa-server.routes.test-models
                          'sa.sa-server.routes.test-routes))
