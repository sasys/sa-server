(ns sa.sa-server.routes.models
  "Describes the routes for our models.
   The pattern for this code is taken from the liberator intro."
  (:require [clojure.java.io            :as io]
            [clojure.data.json          :as json]
            [clojure.edn                :as edn]
            [sa.sa-server.models.model  :as mm]
            [cognitect.transit          :as t]
            [clojure.java.io            :as jio]
            [io.clojure.liberator-transit]
            [clojure.string             :refer [split]]
            [liberator.core             :refer [resource defresource]]
            [compojure.core             :refer [defroutes ANY GET PUT POST DELETE]]
            [taoensso.timbre            :refer [info  warn  error  fatal]])
  (:import (java.io ByteArrayInputStream ByteArrayOutputStream))



  (:import [java.net URL]))


(defn uuid [] (str (java.util.UUID/randomUUID)))


(defn make-model-summary
 "Assumes a map like :meta ... :model ..."
 [request model]
 (let [id (get-in model [:sadf/meta :sadf/uuid])
       uri (mm/make-uri-string (str (name (:scheme request))) (:server-name request) (:server-port request) mm/path id)]
   (-> (:sadf/meta model)
       (assoc :sadf/uri uri)
       (assoc-in [:sadf/detail :sadf/uri] uri))))



(defn body-as-string [ctx]
  (if-let [body (get-in ctx [:request :body])]
    (condp instance? body
      java.lang.String body
      (slurp (io/reader body)))))

;; For PUT and POST parse the body as json and store in the context
;; under the given key.
(defn parse-json [ctx key]
  (try
    (if-let [body (body-as-string ctx)]
      (let [data (json/read-str body)]
        [false {key data}])
      {:message "No body"})
    (catch Exception e
      (error e)
      {:message (str "JSON Failed to parse : " e)})))

;; For PUT and POST parse the body as json and store in the context
;; under the given key.
(defn parse-json->transit [ctx key]
  (try
    (if-let [body (body-as-string ctx)]
      (let [out (ByteArrayOutputStream.)  ;could be 4096
            w   (t/writer out :json-verbose)]
        (t/write w body)
        [false {key (.toString out)}])
      {:message "No body"})
    (catch Exception e
      (error e)
      {:message (str "JSON Failed to parse : " e)})))
            ;in  (jio/input-stream (.getBytes data))])))
            ;out (ByteArrayOutputStream. 4096)])))
-           ;w   (t/writer out :json-verbose)
            ;os (.toString out)
        ;(t/write w data)
-       ;(.toString out)



;; For PUT and POST parse the body as json and store in the context
;; under the given key.
(defn parse-edn [ctx key]
  (try
    (if-let [body (body-as-string ctx)]
      (let [data (edn/read-string (str body))]
        [false {key (if (vector? data) (first data) data)}])
      {:message "No body"})
    (catch Exception e
      (error e)
      {:message (str "EDN Failed to parse : " e)})))


(defn parse-transit [ctx key]
  (try
    (if-let [body (body-as-string ctx)]
      (let  [in (jio/input-stream (.getBytes (str body)))
             r    (t/reader in :json-verbose)
             data (t/read r)]
        [false {key (if (vector? data) (first data) data)}])
      {:message "No body"})
    (catch Exception e
      (error e)
      {:message (str "TRANSIT Failed to parse : " e)})))


(defn parse-body
  "Only do this for put or post"
  [parser-map ctx key]
  (when (#{:put :post} (get-in ctx [:request :request-method]))
    ((parser-map (get-in ctx [:request :headers "content-type"])) ctx key)))


;;  check if the content type is valid
(defn check-content-type [ctx content-types]
  (let [content-type (some #{(get-in ctx [:request :headers "content-type"])} content-types)]
    (if (and (#{:put :post} (get-in ctx [:request :request-method]))
             content-type)
      content-type
      [false {:message "Unsupported Content-Type"}])))


;; a helper to create a absolute url for the entry with the given id
(defn build-entry-url [request id]
 (URL. (format "%s://%s:%s%s/%s"
               (name (:scheme request))
               (:server-name request)
               (:server-port request)
               (:uri request)
               (str id))))


(defresource transittojson []
  :available-media-types ["application/json"]
  :allowed-methods [:post]
  :known-content-type? #(check-content-type % ["application/transit+json"])
  :malformed? #(parse-body {"application/transit+json" parse-transit} % ::data)
  :handle-created (fn [ctx] (::data ctx)))


(defresource jsontotransit []
  :available-media-types ["application/transit+json"]
  :allowed-methods [:post]
  :known-content-type? #(check-content-type % ["application/json"])
  :malformed? #(parse-body {"application/json" parse-json->transit} % ::data)
  :handle-created  (fn [ctx] (::data ctx)))


;; list resources
(defresource list-models []
 :available-media-types ["application/json" "application/edn" "application/transit+json"]
 :allowed-methods [:get]
 :handle-ok #(map (fn [id] (str (build-entry-url (get % :request) (name id))))
                (map (fn [summary] (summary :sadf/uuid))  (mm/summarise-all))))


(defresource get-model [id]
  :available-media-types ["application/json" "application/edn" "application/transit+json"]
  :allowed-methods [:get]
  :exists? (fn [_]
             (let [model (mm/get-model-by-id id)]
               (info (str "Model for id : " id " Found model " model))
               (case model
                 :not-found false
                 nil        false
                 :error     false
                 [true {::old model}])))
  :handle-ok (fn [{data ::old}] data))



 ;; create new-resources
(defresource create-model []
  :available-media-types ["application/json" "application/edn" "application/transit+json"]
  :allowed-methods [:post]
  :known-content-type? #(check-content-type % ["application/json" "application/edn" "application/transit+json"])
  :malformed? #(parse-body {"application/json" parse-json "application/edn" parse-edn "application/transit+json" parse-transit} % ::data)
  :post! #(let [res (mm/new-model (::data %) (:request %))
                id  (last (split (.toString res) #"/"))]
             {::res (.toString res) ::id id})
  :post-redirect? (fn [ctx] {:location (::res ctx)})
  :location #(get % ::res)
  :handle-see-other (fn [ctx] (mm/get-model-by-id (::id ctx))) ;#(::res %)
  :handle-ok #(map (fn [id] (str (build-entry-url (get % :request) (name id))))
                 (map (fn [summary] (summary :sadf/uuid))  (mm/summarise-all)))
  :handle-created (fn [ctx] (mm/get-model-by-id (::id ctx))))


(defresource delete-model [id]
  :available-media-types ["application/json" "application/edn" "application/transit+json"]
  :allowed-methods [:delete]
  :respond-with-entity? true
  :multiple-representations? false
  :delete! (fn [_] {::data {(keyword id) (mm/delete-model id)}})
  :handle-ok ::data)

(defresource update-model [id]
  :available-media-types ["application/json" "application/edn" "application/transit+json"]
  :allowed-methods [:put]
  :known-content-type? #(check-content-type % ["application/json" "application/edn" "application/transit+json"])
  :malformed? #(parse-body {"application/json" parse-json "application/edn" parse-edn  "application/transit+json" parse-transit} % ::new)
  :exists? (fn [_]
             (let [e (mm/get-model-by-id id)]
               (info (str "entry-resource fpr id : " id " Found model " e))
               (case e
                 :not-found false
                 nil        false
                 :error     false
                 [true {::old e}])))
  :conflict? #(not= (-> % ::new :sadf/meta :sadf/uuid) (-> % ::old :sadf/meta :sadf/uuid))
  :can-put-to-missing? true
  :can-post-to-missing? true
  :put! (fn [req] (let [res (mm/update-model id (::new req))]
                   (info (str "entry resource put update  : " id " was " res))
                   (case res
                     :not-found false
                     nil        false
                     :error     false
                     {::entry res})))
  :respond-with-entity? true
  :handle-created (fn [{data ::entry}] data)
  :handle-ok (fn [{data ::old}] data))



(defresource list-model-summaries[]
  :available-media-types ["application/json" "application/edn" "application/transit+json"]
  :allowed-methods [:get]
  :handle-ok #(mm/summarise-all-models (get % :request) (mm/get-summaries)))



(defresource get-model-summary [id]
  :available-media-types ["application/json" "application/edn" "application/transit+json"]
  :allowed-methods [:get]
  :exists? (fn [_]
              (let [model (mm/get-model-by-id id)]
                (info (str "Model for id : " id " Found model " model))
                (case model
                  :not-found false
                  nil        false
                  {::entry model})))

  :handle-ok #(make-model-summary (get % :request) (::entry %)))


(defresource update-model-summary [id]
  :available-media-types ["application/json" "application/edn" "application/transit+json"]
  :allowed-methods [:put]
  :known-content-type? #(check-content-type % ["application/json" "application/edn" "application/transit+json"])
  :malformed? #(parse-body {"application/json" parse-json "application/edn" parse-edn  "application/transit+json" parse-transit} % ::new)
  :exists? (fn [_]
              (let [e (mm/get-model-by-id id)]
                (case e
                  :not-found false
                  nil        false
                  {::entry e})))
  :available-media-types ["application/json" "application/edn" "application/transit+json"]
  :can-put-to-missing? false
  :put! (fn [req] (let [res (mm/update-summary id (::new req))]
                    (case res
                      :not-found {::entry (str id " was not found")}
                      nil        {::entry (str id " was not found")}
                      :error     {::entry (str "Error processing operation on id " id)}
                      {::entry res})))
  :handle-ok #(make-model-summary (get % :request) (::entry %)))


;NOTE THE ORDER MATTERS HERE!!!!!
(defroutes model
    (GET "/api/model/summary" [] (list-model-summaries))
    (GET ["/api/model/summary/:id{[-0-9a-zA-Z]+}"] [id] (get-model-summary id))
    (PUT ["/api/model/summary/:id{[-0-9a-zA-Z]+}"] [id] (update-model-summary id))
    (GET ["/api/model/:id{[-0-9a-zA-Z]+}"] [id] (get-model id))
    (PUT ["/api/model/:id{[-0-9a-zA-Z]+}"] [id] (update-model id))
    (DELETE ["/api/model/:id{[-0-9a-zA-Z]+}"] [id] (delete-model id))
    (GET "/api/model" [] (list-models))
    (POST "/api/model" [] (create-model))
    (POST "/api/jsontotransit" [] (jsontotransit))
    (POST "/api/transittojson" [] (transittojson)))
