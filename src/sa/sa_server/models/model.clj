(ns sa.sa-server.models.model
  (:require  [sa.sa-store.store-factory :as sf]
             [sa.sa-store.store         :as st]
             [sa.sa-server.config       :as cf]
             [clojure.java.io           :as io]
             [clojure.data.json         :as json]
             [clojure.edn               :as edn]
             [taoensso.timbre           :refer [info  warn  error  fatal]])

  (:import [java.net URL]))




;Use the configured store
(def store (sf/get-store cf/store-type))

(def path "/api/model")

(defn uuid [] (str (java.util.UUID/randomUUID)))



(defn make-uri-string [scheme server-name server-port path model-uuid]
  (str scheme
       "://"
       server-name
       ":"
        server-port
       path "/"
       model-uuid))

(defn get-summaries []
  (st/summarise-all store))


(defn summarise-all-models
  "
    Makes a summary for each model
  "
  [request summaries]
  (println ">>>>>> " summaries)
  (for [summary summaries]
    (if-let [id (:sadf/uuid summary)]
      (assoc summary :sadf/uri
        (make-uri-string (str (name (:scheme request)))
                         (:server-name request)
                         (:server-port request)
                         path
                         id)))))


;; a helper to create a absolute url for the entry with the given id
(defn build-entry-url [request id]
 (URL. (format "%s://%s:%s%s/%s"
               (name (:scheme request))
               (:server-name request)
               (:server-port request)
               (:uri request)
               (str id))))
;;;;;

(defn list-model-names
  []
  (st/list-model-names store))

(defn new-model
 "Makes a new model and returns the id"
 [model request]
 (let [id (uuid)
       uri (build-entry-url request id)]
  (st/update-model store id (assoc-in model [:sadf/meta :sadf/uuid] id)) uri))


(defn get-ids
  "Get all the uuids known"
  []
  (st/list-model-names store))

;DEPRECATED
(defn get-model-by-name
  ""
  [name]
  (st/get-model store name))

(defn get-model-by-id
  "We need to find the matching names to the ids first"
 [id]
 (st/get-model store id))

(defn update-model
  "We always ensure the id in the model is the same as the id provided,
   if not, fail.  If the model does not exist for the given id, create a new one."
  [id model]
  (info "Updating model "  id)
  (if (= id (get-in model [:sadf/meta :sadf/uuid]))
    (let [original-model (get-model-by-id id)]
      (if (= :not-found original-model)
        (info (str "update-model : No model with id " id " was found, making a new one  with the given id"))
        (info (str "update-model : Found model with id : " id " it matches given updated model")))
      (st/update-model store id (assoc-in model [:sadf/meta :sadf/uuid] id)))
    (do
      (error (str "update-model : Given id : " id
                  " is not the same same as the updated model id : " (get-in model [:sadf/meta :sadf/uuid]) " request failed."))
      :error)))


(defn summarise-all
  "Get a list of all summaries."
  []
  (st/summarise-all store))


(defn update-summary
  [uuid summary]
  (st/update-summary store uuid summary))

(defn delete-all
  ""
  []
  (st/delete-all store))

(defn delete-model
  [id]
  (info "Deleting model : " id)
  (st/delete-model store id))
