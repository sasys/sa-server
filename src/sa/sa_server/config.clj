(ns sa.sa-server.config
  "
    Server configuration.
  "
  (:require [sa.sa-store.store-factory :as sf]))

;;Select the store subsystem to use.
;(def store-type ::sf/file-store)
(def store-type ::sf/couch-store)
