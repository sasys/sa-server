(ns sa.sa-server.core
  (:require ;[liberator.core :refer [resource defresource]]
            [ring.middleware.params :refer [wrap-params]]
            [liberator.dev :refer [wrap-trace]]
            [sa.sa-server.routes.models :refer [model]]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.adapter.jetty :as jetty])
  (:gen-class))


(def handler
  (-> model
      wrap-params
      ;(wrap-trace :header) ;:ui)  ;THIS FAILS WITH PUT!!!!
      (wrap-cors  :access-control-allow-headers #{"Accept"
                                                  "Accept-Encoding"
                                                  "Accept-Language"
                                                  "authorization"
                                                  "Cache-Control"
                                                  "Connection"
                                                  "Content-Type"
                                                  "Content-Length"
                                                  "Host"
                                                  "Origin"
                                                  "Pragma"
                                                  "Referer"}
                  :access-control-allow-origin #".*"
                  :access-control-allow-methods [:get :put :post :delete :patch])))


(defn -main []
  (jetty/run-jetty handler {:port 3001}))
