(defproject com.sbnl.sa.sa-server "0.1.0-SNAPSHOT"
  :description "Project is a sever side component for the sa project."
  :url ""
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure         "1.10.1"]
                 [liberator                   "0.15.3"]
                 [compojure                   "1.6.1"]
                 [ring/ring-core              "1.8.0"]
                 [ring/ring-jetty-adapter     "1.8.0"]
                 [org.clojure/data.json       "0.2.7"]
                 [ring-cors                   "0.1.13"]
                 [com.sbnl.sa.sa-store        "0.1.1-SNAPSHOT"]
                 [com.sbnl.sa.config          "0.1.0-SNAPSHOT"]
                 [pjstadig/humane-test-output "0.10.0"]
                 [javax.xml.bind/jaxb-api     "2.4.0-b180830.0359"]]


  :plugins [[lein-ring               "0.12.5"]
            [com.palletops/uberimage "0.4.1"]
            [lein-beanstalk          "0.2.7"]
            [lein-modules            "0.3.11"]
            [lein-pprint             "1.1.2"]
            [lein-ancient            "0.6.15"]]

  :src ["src"]

  :ring {:handler sa.sa-server.core/handler}
  :aot  [sa.sa-server.core]
  :main sa.sa-server.core
  :profiles {:dev  {:env {:sa-store-host "localhost"
                          :sa-store-port "3450"
                          :sa-sever-store  "sa.sa-store.store-factory/file-store"
                          :sa-client-store "sa.sa-store.store-factory/rest-store ,
                                            sa.sa-store.store-factory/key-store"}}
             :aws-dev  {:env {:sa-store-host "http://ec2-52-57-72-228.eu-central-1.compute.amazonaws.com/"
                              :sa-store-port "80"
                              :sa-sever-store  "sa.sa-store.store-factory/file-store"
                              :sa-client-store "sa.sa-store.store-factory/rest-store ,
                                                sa.sa-store.store-factory/key-store"}}
             :test {:env {:sa-store-host "test-host-1"
                          :sa-store-port "8080"
                          :sa-sever-store  "sa.sa-store.store-factory/memory-store"
                          :sa-client-store "sa.sa-store.store-factory/rest-store,
                                            sa.sa-store.store-factory/key-store"}}})
