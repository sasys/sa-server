#
#
#
# Docker file describes the container for the sa project REST service
#
FROM sbnl-clj-base:0.0.1

ARG PORT
ENV PORT $PORT:-6061
ENV DB_HOST store.sa.softwarebynumbers.com
ENV DB_PORT 5984
ENV DB_NAME sa
ENV DB_USER admin
ENV DB_PASSWORD password

WORKDIR /usr/src/app
COPY sa-server.jar /usr/src/app
COPY project.clj /usr/src/app

CMD ["java", "-cp", "sa-server.jar", "clojure.main", "-m", "sa.sa-server.core"]
