

# sa-server

[![CircleCI](https://circleci.com/bb/sasys/sa-server/tree/master.svg?style=svg)](https://circleci.com/bb/sasys/sa-server/tree/master)
[![codecov](https://codecov.io/bb/sasys/sa-server/branch/master/graph/badge.svg)](https://codecov.io/bb/sasys/sa-server)





A REST server for the sa project.  Uses Liberator to provide a model api.

## API

### Use Of CURL To Manage Models

In the resources/test directory

#### GET
On the collection returns a list of urls of the entities.
##### A list of models
```
http://localhost:3000/api/model

```
##### A Specific model
On a specific model returns a specific entity.
```
curl http://localhost:3000/api/model/14154
```

#### POST

##### A New Model

```
curl -X POST -d @post10.edn   -H "Content-Type: application/edn" localhost:3000/api/model
```

```
curl -X POST -d @post1.json   -H "Content-Type: application/json" localhost:3000/api/model
```

```
curl -X POST -d @a.transit   -H "Content-Type: application/transit+json" localhost:3000/api/model
```

#### PUT

##### Update An Existing model

```
curl -d @   PUT
```

#### DELETE

##### Delete An Existing Entry
```
curl -X DELETE  localhost:3000/api/model/23017
```


## API Design

### Operations

|URL|Method|Supported Mime Types|Returns|Notes/Examples|
|---|---|---|---|---|
|/api/model|GET, POST|application/json, application/edn|A list of model names and ids or a new model.|List available models|
|/api/model/id|GET, PUT, DELETE|application/json, application/edn|A specific model, a model update or a model delete.|A specific model|
|/api/model/summary|GET|application/json, application/edn|List model summaries.||
|/api/model/summary/id|PUT|application/json, application/edn |Update the summary of a specific model.| |
||||||
|/api/dump|GET|||Just dumps the whole model database.  Test only.|

## Building And running

## Local circle ci

    circleci local execute --job sa-server


### Unit Test Locally

    clojure -A:test

### Local Coverage

    clojure -A:coverage


### Standalone executable

Make an executable jar file
```
clojure -A:depstar

java -cp sa-server.jar clojure.main -m sa.sa-server.core
```




### Docker

We need a bridge network called sa

    docker network ls
    2d5f64c0c5b5        sa                  bridge              local


We need a couch db instance running on the sa network

    docker ps -a

    DEPRECATED BEACUSE COUCHDB RETROSPECTIVELY CHANGED TO REQUIRE THIS. docker run --name=razor --network=sa -p 5984:5984 -d couchdb:latest

    docker run --name=razor --network=sa -e DB_USER=admin -e DB_PASSWORD=password -p 5984:5984 -d couchdb:latest

    docker network inspect sa



Use the supplied docker file to build the container :

Make sure the base image is built

    sbnl-clj-base:0.0.1

```
clojure -A:depstar

docker build . -t sa-server:0.0.1
```
Start the docker container :
```
docker run --name=sa-server -i -t -p80:3000 sa-server:0.0.1
```
or, on a network we specify the database with:

```
 sa-server.sa - the name of the running container.the name of the network

```

IF RUNNING WITH THE NGINX PROXY :
```
docker run --name=sa-server --env DB_HOST=razor.sa --env DB=sadebug --env DB_USER=admin --env DB_PASSWORD=password --network=sa -i -t -p3000:3000 sa-server:0.0.2

```

NOTE that the db host must be the same as the couch db host (minus the sa)


### Minikube

Create the file space in minikube

    minikube ssh
    mkdir /tmp/sa-data
    chmod ugo+rwx /tmp/sa-data

Create the namespace

    kubectl create namespace sa

Create the deployment

    kubectl create --namespace=sa -f sa-server-local.yaml

Make the service available via a node port

    kubectl expose deployment sa-server --name=sa-server-exposed  --type=LoadBalancer -n sa
    minikube service sa-server -n sa


## License

Copyright © 2017 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
